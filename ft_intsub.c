/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_intsub.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vbudnik <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/30 20:14:57 by vbudnik           #+#    #+#             */
/*   Updated: 2017/11/30 21:58:48 by vbudnik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

static int		ft_count(int	*int_mas);

int		*ft_intsub(int *array, unsigned int start)
{
	int		*res;
	size_t	i;
	int		len;

	i = 0;
	while (array[len] != -1)
		len++;
	if (array == NULL)
		return (NULL);
	res = (int *)malloc(sizeof(int) * (len + 1));
	if (res == NULL)
		return (NULL);
	while (len--)
	{
		res[i] = array[start + i];
		i++;
	}
	return (res);
}

static int		ft_count(int *int_mas)
{
	int		i;

	while (int_mas[i] != -1)
		i++;
	return (i);
}
