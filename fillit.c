/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fillit.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vbudnik <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/24 21:10:04 by vbudnik           #+#    #+#             */
/*   Updated: 2017/12/10 17:45:04 by vbudnik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */
#include <unistd.h>
#include  <stdlib.h>
#include <stdio.h>
#include "libft/libft.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include "fillit.h"

char	*ft_readmap(char **arr);
void	ft_error(void);
int		ft_chiking(char *str);
int		*ft_position(char *str);
void	ft_mesagen(char *str);
size_t	ft_count_sign(const char *s);
int		ft_count_elem(const int *elem);
int 	*ft_mapfour(int *mapi);
int		*ft_intmap(int *arr_int);
int		*ft_separator(int *separ);
char	*ft_del_new_line(char *str);
int		ft_count_newline(char *str);
int		**ft_intsplit(const int *arr_mas);
int		ft_count_model(const int *arr_mas);
int		ft_lenint(const int *mass);
//char	**ft_printarr(size_t j);
int		ft_size_pole(int *posit);
int		**ft_intset(int **b, int c, size_t len);
int		**ft_int_memalloc(size_t size);
int		ft_count_string(const char *arr, int size);
char	*ft_sepsub(char *str, int start);
char	**ft_separ_arr(char *mass, int size);
void	ft_print_map(char *map, int size);
char	*ft_printarr(int *posit, char *map, int size);
int		*ft_ret_pos(int **separ, int count_model);
char	*ft_map_creat(int size);

int		main(int argc, char **argv)
{
	int		size;
//	int		j;
	int		**split;
	char	*str;
//	char	**map_map;
	int		*map;
	char	*del_new_line;

	if (argc != 2)
	{
		ft_error();
		return (0);
	}

	str = ft_readmap(&argv[1]);
	if ( ft_chiking(str) == 0)
	{
		ft_mesagen("Not valind file");
		return (0);
	}
	del_new_line = ft_del_new_line(str);
	map = ft_position(del_new_line);

	printf("\nAll good\n");
	size = ft_size_pole(map);
	printf("SIZE --->>>%d \n", size);
	split = ft_intsplit(map);

	char	*lol;
	int		*ret_pos;
	int		count_model;
	int		i;
    char    *create_map;

	count_model = ft_count_model(map);
	ret_pos = ft_ret_pos(split, size);
    create_map = ft_map_creat(size);

	printf("\nMap creat --->>>\n%s\n", create_map);
	printf("\nstrlen - create_map --->>>  %zu\n", ft_strlen(create_map));

	lol = ft_printarr(ret_pos,create_map, size);
	ft_print_map(lol, size);
	printf("\nLOL --->>> \n%s\n", create_map);
	printf("\nstrlen LOL --->>> %zu \n", ft_strlen(lol));

/*
	i = 0;
	while (i < size * 4)
	{
		printf("map[i] ==>> %d, ", ret_pos[i]);
		i++;
	}
//	printf("MAP --->>> \n%d", ret_pos);
*/
	return (0);
}

/*
** work
*/

char	*ft_readmap(char **arr)
{
	int		fd;
	char	buf[1];
	static char	str[5200];
	int		i;

	i = 0;
	fd = open (*arr, O_RDONLY);
	while (read(fd, &buf, 1))
	{
		str[i] = buf[0];
		i++;
	}
	close(fd);
	str[i] = '\0';
	return (str);
}

/*
** work
*/

void		ft_error(void)
{
	ft_putendl("ERROR");
}

/*
**work
*/

int		ft_chiking(char *str)
{
	while (*str)
	{
		if (*str != '.' && *str != '#' && *str != '\n')
		{
			return (0);
		}
		str++;
	}
	return (1);
}

int		*ft_position(char *str)
{
	int		i;
	int		j;
	int		*map_position;

	i = 0;
	j = 0;
	map_position = (int	*)malloc(sizeof(int) * ft_count_sign(str) + 1);
	if (map_position == NULL)
		return (NULL);
	while (str[i] != '\0')
	{
		if (str[i] == '#')
		{
			map_position[j] = i;
			j++;
		}
		i++;
	}
	map_position[j] = -1;
	return (map_position);
}

/*
**work
*/

void	ft_mesagen(char *str)
{
	ft_putendl(str);
}

size_t	ft_count_sign(char const *s)
{
	size_t	i;

	i = 0;
	while (*s)
	{
		if (*s == '#')
			i++;
		s++;
	}
	return (i);
}

int		ft_count_elem(const int *elem)
{
	int		i;

	i = 0;
	while (elem[i] != -1)
		i++;
	return (i);
}

int		*ft_mapfour(int *mapi)
{
	int		i;
	int		*mapi_res;

	i = 0;
	mapi_res = 0;
	while (mapi[i] != -1)
	{
		mapi_res[i] = mapi[i] / 4;
		i++;
	}
	return (mapi_res);
}

int		*ft_separator(int *separ)
{
	int		i;

	i = 0;
	while (separ[i] != -1)
	{
		if (i > 4 && (i % 4) == 0)
		{
			separ[i] = separ[i + 1] ;
			i++;
		}
		i++;
	}
	return (separ);
}

char	*ft_del_new_line(char *str)
{
	int		i;
	int		j;
	char	*new_str;
	int		len;

	i = 0;
	j = 0;
	len = ft_strlen(str) - ft_count_newline(str);
	new_str = (char *)malloc(sizeof(char) * len +1);
	while (str[i] != '\0')
	{
		if (str[i] == '\n')
			i++;
		new_str[j] = str[i];
		i++;
		j++;
	}
	new_str[j] = '\0';
	return (new_str);
}

int		ft_count_newline(char *str)
{
	int		i;
	int		count;

	count = 0;
	i = 0;
	while (str[i] != '\0')
	{
		if (str[i] == '\n')
			count++;
		i++;
	}
	return (count);
}

int		**ft_intsplit(const int *arr_mas)
{
	int		index;
	int		nb_word;
	int		**temp;

	index = 0;
	nb_word = ft_count_model((const int *)arr_mas);
	temp = (int **)malloc(sizeof(int *) * (ft_count_model(arr_mas) + ft_count_elem(arr_mas) + 1));
	if (temp == NULL)
		return (NULL);
	while (nb_word)
	{
		temp[index] = ft_intsub((const int *)arr_mas, 0, ft_lenint(arr_mas));
		if (temp[index] == NULL)
			return (NULL);
		arr_mas = arr_mas + ft_lenint(arr_mas);
		index++;
		nb_word--;
	}
	return (temp);
}

int		ft_count_model(const int *arr_mas)
{
	int		cou;

	cou = 0;
	cou = ft_count_elem(arr_mas) / 4;
	return (cou);
}

int		ft_lenint(const int *mass)
{
	int		len;

	len = 0;
	while (*mass != -1)
	{
		len++;
		mass++;
		if (len == 4)
			return (len);
	}
	return (len);
}

int		ft_len_str(char *mass,int size)
{
	int		len;

	len = 0;
	while (*mass != '\0')
	{
		len++;
		mass++;
		if (len == size)
			break ;
	}
	return (len);
}
/*
char	**ft_printarr(size_t j)
{
	size_t	n;
	size_t	i;
	char	**map;

	n = 0;
	i = 0;
	while (i * i <= 4 * j)
		i++;
	if (!(map = (char **)ft_memalloc(sizeof(char *) * (i + 1))))
		return (NULL);
	map[i] = 0;
	while (n < i)
	{
		map[n] = (char *)ft_memalloc(sizeof(char *) * (i + 1));
		j++;
	}
	n = 0;
	while(map[n])
	{
		ft_memset(map[n], '.',sizeof(char ) * i);
		n++;
	}
	return (map);
}
*/
int		ft_size_pole(int *posit)
{
	int		i;
	int		n;

	i = 0;
	n = ft_count_model(posit);
	while (i * i < n)
		i++;
	return (i);
}

char	**ft_separ_arr(char *mass, int size)
{
	int		index;
	int		nb_word;
	char	**temp;

	index = 0;
	nb_word = ft_count_string(mass, size);
	temp =(char **)malloc(sizeof(char) *(ft_count_string(mass, size) + ft_strlen(mass) + 1));
	if (temp == NULL)
		return (NULL);
	while (nb_word)
	{
		temp[index] = ft_sepsub(mass, 0);
		if (temp[index] == NULL)
			return (NULL);
		mass = mass + ft_len_str(mass, size);
		index++;
		nb_word--;
	}
	return (temp);
}

int		ft_count_string(const char *arr, int size)
{
	int		i;

	i = 0;
	while (arr[i] != '\0')
		i++;
	i /= size;
	return (i);
}

char	*ft_sepsub(char *str, int start)
{
	char	*res;
	int		i;
	int		len;

	i = 0;
	len = ft_strlen(str);
	if (str == NULL)
		return (NULL);
	res = (char *)malloc(sizeof(char) * (len + 1));
	if (res == NULL)
		return (NULL);
	while (len--)
	{
		res[i] = str[start + i];
		i++;
	}
	res[i] = '\n';
	return (res);
}

char	*ft_map_creat(int size)
{
	int 	i;
	int		j;
	int		len;
	char	*map;

	i = 0;
	j = 0;
	len = size * 4;
	map = (char *)ft_memalloc(sizeof(char) * len);
	while (i < len * len * 4)
	{
		j = 0;
		while (j < len * len)
		{
			map[i] = '.';
			j++;
		}
		i++;
	}
	map[i] = '\0';
	return (map);
}

void	ft_print_map(char *map,int size)
{
	int	i;
	int j;

	j = 0;
	i = 0;
	while (i < size )
	{
		j = 0;
		while (j < size)
		{
			ft_putstr(&map[i]);
			ft_putchar('\n');
			j++;
		}
		i++;
	}
}

char	*ft_printarr(int *posit,char *map, int size)
{
	int		i;
	int		j;
	int		len;
	char	*res;

	i = 0;
	j = 0;
	len = size * size;
	if (len == 1)
		len = 2;
	res = ft_strnew(len);
	while (i != len)
	{
		if (i == posit[j])
		{
			res[i] ='A';
			j++;
		}
		else
			res[i] = '.';
		i++;
	}
	res[i] = '\0';
	return (res);
}

int		*ft_ret_pos(int **separ, int count_model)
{
	int		i;
	int		j;
	int		k;
	int		*res;

	i = 0;
	j = 0;
	k = 0;
	res = (int *)malloc(sizeof(int) * (count_model * 4 + 1));
	while (i < count_model)
	{
		j = 0;
		while (j < 4)
		{
			res[k] = separ[i][j];
			j++;
			k++;
		}
		i++;
	}
	res[k] = -1;
	return (res);
}
